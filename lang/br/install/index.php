<?
$MESS["CAL_MODULE_NAME"] = "Calendário de Eventos";
$MESS["CAL_MODULE_DESCRIPTION"] = "Calendário de Eventos";
$MESS["CAL_PHP_L439"] = "Você está usando a versão PHP #VERS# enquanto que o módulo requer a versão 5.0.0 ou superior. Por favor, atualize sua instalação PHP ou entre em contato com o suporte técnico.";
$MESS["CAL_INSTALL_TITLE"] = "Instalação do módulo de calendário de eventos";
$MESS["CAL_UNINSTALL_TITLE"] = "Desinstalação do módulo de calendário de eventos";
$MESS["CAL_GO_CONVERT"] = "Converter calendários de eventos";
$MESS["CAL_DEFAULT_TYPE"] = "Eventos";
?>