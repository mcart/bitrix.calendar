<?
$MESS["EC_NS_INVITE"] = "Notificação de convite de reunião";
$MESS["EC_NS_REMINDER"] = "Notificação próxima reunião";
$MESS["EC_NS_CHANGE"] = "Notificação de atualizações de reunião";
$MESS["EC_NS_INFO"] = "Nota de aceitação ou rejeição do convite de reunião";
$MESS["EC_NS_EVENT_COMMENT"] = "Novo comentário sobre um evento";
?>